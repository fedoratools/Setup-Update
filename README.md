## Automatizare actualizare sistem

Acest script Bash automatizează procesul de actualizare a sistemului folosind `flatpak` și `dnf` pe distribuțiile bazate pe Fedora. 

### Instrucțiuni

1. Descărcați scriptul și salvați-l local.
2. Rulați scriptul bash în terminal.
3. Scriptul va verifica alias-ul `update` în fișierul `~/.bashrc` și îl va adăuga dacă nu există deja.
4. Va crea o regulă sudoers specifică pentru utilizatorul curent pentru a permite actualizarea fără a introduce parola pentru `dnf`.
5. Dacă regula nu există deja, o va adăuga în fișierul `/etc/sudoers` folosind `visudo`.
6. Scriptul va reîncărca fișierul `~/.bashrc` pentru a aplica modificările.

### Utilizare

Asigurați-vă că rulați scriptul cu privilegii adecvate, de preferință ca utilizator cu permisiuni sudo.

```bash
./update_system.sh
```

### Rețineți

- Scriptul poate fi rulat periodic sau integrat în alte procese de automatizare a sistemului.
- Asigurați-vă că înțelegeți consecințele adăugării unei reguli sudoers fără parolă înainte de a rula acest script.

### Wiki

[Wiki](https://gitlab.com/fedoratools/Setup-Update/-/wikis/Setup-Update)