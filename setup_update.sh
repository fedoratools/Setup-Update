#!/bin/bash

# Numele utilizatorului curent
USER=$(whoami)

# Adaugă alias-ul în ~/.bashrc dacă nu există deja
if ! grep -q "alias update='flatpak update --noninteractive && sudo dnf upgrade --refresh -y'" ~/.bashrc; then
    echo "alias update='flatpak update --noninteractive && sudo dnf upgrade --refresh -y'" >> ~/.bashrc
    echo "Alias-ul 'update' a fost adăugat în ~/.bashrc"
else
    echo "Alias-ul 'update' există deja în ~/.bashrc"
fi

# Creează o regulă sudoers specifică utilizatorului curent
SUDOERS_RULE="$USER ALL=(ALL) NOPASSWD: /usr/bin/dnf"

# Verifică dacă regula există deja
if sudo grep -q "^$SUDOERS_RULE" /etc/sudoers; then
    echo "Regula pentru 'dnf' fără parolă există deja în sudoers"
else
    # Adaugă regula direct în sudoers folosind visudo
    echo "$SUDOERS_RULE" | sudo EDITOR='tee -a' visudo
    if [ $? -eq 0 ]; then
        echo "Regula pentru 'dnf' fără parolă a fost adăugată"
    else
        echo "Eroare la adăugarea regulii în sudoers. Modificările nu au fost aplicate."
    fi
fi

# Reîncarcă ~/.bashrc
source ~/.bashrc
echo "~/.bashrc a fost reîncărcat"

